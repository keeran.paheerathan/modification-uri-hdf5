import h5py
import argparse
import pandas as pd
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument("--path", help="Experiments Path", required=True, dest="path")
parser.add_argument(
    "--uri_path", help="Experiments URI Path", required=True, dest="uri_path"
)
args = parser.parse_args()


path = args.path

# Load Experiment URIs
pd_XP = pd.read_excel(args.uri_path, sheet_name="XP")
xp_dict = dict(zip(pd_XP.XP, pd_XP.NewURI))

# Load Sensor URIs
pd_sensor_uri = pd.read_excel(args.uri_path, sheet_name="Devices")
sensor_uri_dict = dict(zip(pd_sensor_uri.name, pd_sensor_uri.NewURI))

# Load MicroPlot URIs
pd_microplot_22TO01_CIMSon = pd.read_excel(args.uri_path, sheet_name="OS_22TO01_CIMSon")
pd_microplot_22TO021_CIMSon = pd.read_excel(
    args.uri_path, sheet_name="OS_22TO021_CIMSon"
)
pd_microplot_22TO022_CIMSon = pd.read_excel(
    args.uri_path, sheet_name="OS_22TO022_CIMSon"
)

microplot_URIs = {
    "22TO01_CIMSon": dict(
        zip(pd_microplot_22TO01_CIMSon["Nom OS"], pd_microplot_22TO01_CIMSon.NewURI)
    ),
    "22TO021_CIMSon": dict(
        zip(pd_microplot_22TO021_CIMSon["Nom OS"], pd_microplot_22TO021_CIMSon.NewURI)
    ),
    "22TO022_CIMSon": dict(
        zip(pd_microplot_22TO022_CIMSon["Nom OS"], pd_microplot_22TO022_CIMSon.NewURI)
    ),
}

count = 0
# Replace URIs
for path in Path(path).rglob("*.h5"):
    count += 1
    with h5py.File(path, "r+") as f:
        keys = list(f["Session1"].keys())

        for xp, uri in xp_dict.items():
            if xp in str(path):
                f["MetaData"]["TrialInformation"].attrs["ExperimentURI"] = uri.encode(
                    "UTF-8"
                )

            for microplot_URI in microplot_URIs[xp]:
                if microplot_URI in str(path):
                    f["Session1"][keys[0]].attrs["MicroPlotURI"] = microplot_URIs[xp][
                        microplot_URI
                    ]

        f["Session1"][keys[1]].attrs[
            "EquipmentURI"
        ] = b"phenotoul:id/device/field-robot-v1801"
        f["Session1"][keys[1]]["Head1"].attrs[
            "HeadURI"
        ] = b"phenotoul:id/device/field-robot-v1801"

        head_keys = list(f["Session1"][keys[1]]["Head1"])

        # Replace Sensor URIs
        for key in head_keys:
            try:
                f["Session1"][keys[1]]["Head1"][key].attrs[
                    "SensorURI"
                ] = sensor_uri_dict[key].encode("UTF-8")
            except AttributeError:
                f["Session1"][keys[1]]["Head1"][key].attrs["SensorURI"] = b""

if count == 0:
    raise ValueError("No HDF5 files were found.")
else:
    print(str(count) + " files were edited")

# Vérification
# with h5py.File(URL, 'r') as f:
#     keys = list(f["Session1"].keys())

#     print(f["MetaData"]["TrialInformation"].attrs["ExperimentURI"])
#     print(f["Session1"][keys[0]].attrs["MicroPlotURI"])

#     print(f["Session1"][keys[1]].attrs["EquipmentURI"])
#     print(f["Session1"][keys[1]]["Head1"].attrs["HeadURI"])

#     head_keys = list(f["Session1"][keys[1]]["Head1"])
#     print(len(head_keys))

#     for key in head_keys:
#         print(f["Session1"][keys[1]]["Head1"][key].attrs["SensorURI"])
